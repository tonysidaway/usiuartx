; Register names for nga_avr

#define SIGN_R r0
#define NGAMASK_R r1

#define TEMPL_R r2
#define TEMPH_R r3
#define TEMPW_R TEMPL_R

#define COUNT_R r16
#define USI_UART_STATUS_R r17

#define TOSL_R r24
#define TOSH_R r25
#define TOSW_R TOSL_R
