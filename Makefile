uartx.o: uartx.s
	avr-gcc -c -nostartfiles -mmcu=attiny85   uartx.s -o uartx.o

uartx.s: uartx.S
	avr-gcc -S -nostartfiles -mmcu=attiny85   uartx.S > uartx.s

clean:
	-rm -f uartx.s uartx.o
